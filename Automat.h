//
// Created by acer on 26.11.2020.
//

#ifndef AUTOMAT_AUTOMAT_H
#define AUTOMAT_AUTOMAT_H
#include "Napoj.h"

using namespace std;
class Napoj;
class Automat
{

    friend class Napoj;
    int liczba_napoi[4];

    Napoj n1;
    Napoj n2;
    Napoj n3;
    Napoj n4;

public:
    void ilosc_cena();
    void wybierz_napoj();
    bool wrzuc_monete(Napoj n);
    void sprawdz(Napoj n);

    Automat(): liczba_napoi{3,3,3,3}, n1("Pepsi",3.7,0), n2("Coca Cola",2,1),n3("Goraca czekolada",10,2),n4("Kawa",6.1,3)
    {};// konstruktor Automat wraz z deklaracją obiektów zaprzyjaźnionej klasy Napoj

    ~Automat();


};

#endif //AUTOMAT_AUTOMAT_H
